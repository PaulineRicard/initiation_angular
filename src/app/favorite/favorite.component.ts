import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  @Input() count:number;
  @Output() MajCount: EventEmitter<number> = new EventEmitter<number>();
  @Input() favorite = [];
  @Output() delFav: EventEmitter<[]> = new EventEmitter <[]>();
  constructor() { }

  ngOnInit() {
  }

  removeFav(id){
    this.favorite.splice(id, 1);
    this.count = --this.count;
    //* console.log(this.favorite);
    // @ts-ignore
    this.delFav.emit(this.favorite);
    this.MajCount.emit(this.count);

  }


}
