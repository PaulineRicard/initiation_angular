import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title:string = 'beweb-movies';
  public count:number = 0;
  public favorite:[] = [];

  count2Modif($event){
    this.count = $event;
    console.log(this.count);
  }

  loadFavorite($event){
    this.favorite = $event;
    //*console.log(this.favorite);
  }

 
  // test2(){
  //   console.log(this.count);
  // }
}

