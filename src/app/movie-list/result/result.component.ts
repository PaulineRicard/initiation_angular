import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

private saveFavorite: any;
  @Input() count: number;
  @Output() countC2: EventEmitter<number> = new EventEmitter<number>();
  @Input() moviesList:[];
  @Input() favorite;
  @Output() transfertFavoris: EventEmitter<[]> = new EventEmitter<[]>();

  constructor() { }

  ngOnInit() {
  }
//* appelée au clic sur le bouton "ajouter aux favoris"
//todo mettre en place le compteur: variable count dans app + output result+movie list => input navbar
//todo eventemitter pour envoyé la variable count ds la parent
//todo eventemitter pour envoyé la liste des favoris dans le parent
addNewFavorite(poster, title, overview){
  this.countC2.emit(++this.count);
    this.saveFavorite = {poster, title, overview};
    (this.favorite).push(this.saveFavorite)
    // @ts-ignore
    this.transfertFavoris.emit(this.favorite);
    //* console.log(this.favorite);
    //* console.log(this.saveFavorite);
      //* console.log(this.count);
  }
}
