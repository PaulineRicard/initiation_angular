import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
//import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {


  @Input() count: number;
  @Output() countC: EventEmitter<number> = new EventEmitter<number>()
  @Input() favorite: [];
  @Output() sendFav: EventEmitter<[]> = new EventEmitter<[]>();
  public moviesList = [];
  constructor() { }

  ngOnInit() {
  }

  test() {
    console.log(this.moviesList);
  }
  //* appelée lorsque l'event list existe(voir movie-list.component.html) -> permet de mettre a jour moviesList!!
  moviesListChange($event) {
    this.moviesList = $event;
  }

  //* permet de modifier la valeur du compteur et de l'envoyé au parent! 
  countModif($event){
    this.count = $event;
    this.countC.emit(this.count);
  }

  transfertFav($ev){
    //*console.log(this.favorite);
    //*console.log($ev);
    this.favorite = $ev;
    this.sendFav.emit(this.favorite);
  }
}
