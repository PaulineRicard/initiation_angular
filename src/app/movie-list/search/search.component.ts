import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MovieService } from '../../service/movie.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  filter = '';

  constructor(private movie: MovieService) { }

  @Input() moviesList: [];
  @Output() list: EventEmitter<[]> = new EventEmitter<[]>();

  ngOnInit() {
  }
  //todo importer le service movie
  //todo variable moviesList a definir dans le parent + @input() pour le récuperer
  //todo @output list + eventEmitter pour la renvoyée dans le parent!!
  search(filter) {
    //? console.log(filter);
    this.movie.find(filter).subscribe(

      (response) => {
        //* console.log(this.moviesList);
        this.moviesList = response['results']
        this.list.emit(this.moviesList);
       //*  console.log(this.moviesList);
      }
    )
  }
}

